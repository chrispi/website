---
title: Über uns
date: 2023-01-03T20:34:36.230Z
lastmod: 2023-01-31T07:25:09.747Z
tags:
  - ""
categories:
  - ""
slug: about
---

Fedimins ist ein Kollektiv aus Administratoren und Moderatoren welches mehrere Ziele verfolgt:

- Austausch über die Arbeit als Administrator_in/Moderator_in
- Einen Grundstock an Regeln festzulegen

Damit möchten wir Fedimins als eine Art Label etablieren, welches sowohl Usern als auch Administrator_inen/Moderator_inen für Vertrauen sorgt. Dafür erstellen wir ein Manifest, welche Grundregeln für Instanzbetreiber bereitstellt.

Vielleicht ist dem ein oder anderen https://www.chatons.org ein Begriff, persönlich finde ich(Tealk) es eine ausgezeichnete Idee und denke das #Fedimins ebenfalls in diese Richtung einschlagen könnte.
