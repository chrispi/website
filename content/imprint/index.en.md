---
title: Imprint
date: 2023-01-03T20:34:36.230Z
lastmod: 2023-01-19T17:05:54.962Z
tags:
  - ""
categories:
  - ""
slug: imprint
---

## Service provider

Daniel Buck<br/>
<span style="font-size: 10px;">Herzog von Meranien</span><br/>
Kirchenstr. 2<br/>
92693 Eslarn

## Contact

**E-mail address:** [webmaster@fedimins.net](mailto:webmaster@fedimins.net)<br/>
**Phone:** +49 156 78563777<br/>

## Liability and proprietary notices.

**Disclaimer**: The contents of this online offer have been prepared carefully and according to our current knowledge, but are for information purposes only and do not have any legally binding effect, unless it is legally binding information (e.g. the imprint, privacy policy, terms and conditions or mandatory instructions of consumers). We reserve the right to change or delete the contents in whole or in part, provided that contractual obligations remain unaffected. All offers are subject to change and non-binding.

**Links to external websites**: Contents of external websites to which we refer directly or indirectly are outside our area of responsibility and we do not adopt them as our own. The provider of the linked websites is solely liable for all contents and in particular for damages resulting from the use of the information available on the linked websites.

**Copyrights and trademark rights**: All content displayed on this website, such as texts, photographs, graphics, brands and trademarks are protected by the respective protective rights (copyrights, trademark rights). The use, reproduction, etc. are subject to our rights or the rights of the respective authors or rights managers.

**Notices of legal violations**: If you notice any legal violations within our internet presence, we ask you to point them out to us. We will remove illegal content and links immediately after becoming aware of them.

[Created with free Datenschutz-Generator.de by Dr. Thomas Schwenke](https://datenschutz-generator.de "Legal text by Dr. Schwenke - please click for more information.")
