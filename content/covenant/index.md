---
title: Serverabkommen
description: Das ist die vorab Version des Fedimins Serverabkommens
date: 2020-09-01T00:00:00.000Z
lastmod: 2023-01-16T17:51:24.091Z
tags:
  - ""
categories:
  - ""
slug: covenant
---

Fedimins ist ein Kollektiv von alternativen, transparenten, offenen, neutralen und solidarischen Hosting-Anbietern. Jedes Mitglied dieses Kollektivs, im Folgenden "Fedimin" genannt, verpflichtet sich, die vorliegende Charta zu respektieren. Was nicht explizit durch diese Charta oder das Gesetz verboten ist, ist erlaubt. Der Fedimin kann jede beliebige Rechtsform haben: eine juristische Person oder eine natürliche Person (eine Privatperson, ein Unternehmen, ein Verein, eine SCOP usw.).

**Anforderungen**

- Ein Fedimin verpflichtet sich, mit Respekt und Wohlwollen gegenüber den anderen Mitgliedern des Kollektivs zu handeln
- Ein Fedimin verpflichtet sich, mit Respekt und Wohlwollen gegenüber den Nutzer_innen[^1] ihrer Dienste zu handeln

## Basisverpflichtungen

### Hosting

- Ein Fedimin muss Administratorzugriff (Root-Zugriff) auf das Betriebssystem haben, das die endgültigen Online-Dienste betreibt
  - Im Falle der Anmietung von Servern (virtuell oder dediziert) muss der Fedimin sicherstellen, dass der Anbieter sich vertraglich verpflichtet, nicht auf die Daten zuzugreifen
- Ein Fedimin verpflichtet sich, dass mindestens zwei verschiedenen (natürliche) Personen der Administratorzugriff (Root-Zugriff) auf das Betriebssystem ermöglicht wird.
- Ein Fedimin verpflichte sich, seine Nutzer_innen über den Grad der Kontrolle, die er über seine Infrastruktur hat, zu informieren
- Ein Fedimin verpflichtet sich, seinen Kontrollgrad über die Hardware und Software, die die Dienste und die damit verbundenen Daten beherbergt, öffentlich und unmissverständlich anzuzeigen. Insbesondere muss der Name des Hosting-Anbieter[^2], der die Server physisch hostet, gegenüber den Nutzer_innen klar angegeben werden
- Ein Fedimin verpflichtet sich, die Backup-Politik zu implementieren, welche tägliche Backups ermöglicht
- Ein Fedimin verpflichtet sich, ausschließlich freie Distributionen (GNU/Linux, FreeBSD, etc.) als Betriebssystem für die Infrastruktur der Nutzer_innen Dienste zu verwenden
- Ein Fedimin verpflichtet sich, alle Informationen (Konten und persönliche Daten) über den_r Nutzer_innen auf dessen Wunsch im Rahmen der gesetzlichen und technischen Verpflichtungen endgültig zu löschen.

### Transparenz

- Ein Fedimin verpflichtet sich, ihre technische Infrastruktur (Distribution und verwendete Software) transparent zu machen. Aus Sicherheitsgründen können bestimmte Informationen (z. B. die Versionsnummer der Software) nicht öffentlich zugänglich gemacht werden
- Ein Fedimin verpflichtet sich, sein Bestes zu tun, um die Sicherheit seiner Infrastruktur zu gewährleisten
- Ein Fedimin verpflichtet sich, keine Eigentumsrechte an Inhalten, Daten und Metadaten zu beanspruchen, die von den Nutzer_innen produziert werden
- Ein Fedimin verpflichtet sich, sichtbare, klare, unmissverständliche und allgemein verständliche Nutzungsbedingungen zu veröffentlichen
- Ein Fedimin verpflichtet sich, mindestens 3 Monate vor Abschaltung der Dienste die Nutzer_innen zu benachrichtigen

### Datenschutz

- Ein Fedimin verpflichtet sich, die Grundfreiheiten seiner Nutzer_innen, insbesondere den Schutz ihrer Privatsphäre, bei jeder seiner Handlungen zu priorisieren
- Ein Fedimin verpflichtet sich, keine Tracker einzubauen (d.h. keine Seiten auszuliefern, die Inhalte von anderen Websites nachladen – ausgenommen sind Inhalte, die von Teilnehmenden gepostet wurden). Sponsoring oder Mäzenatentum durch die Anzeige der Identität von Partnerstrukturen (Name, Logo usw.) ist erlaubt, vorausgesetzt, dass keine persönlichen Informationen an die Partner weitergegeben werden
- Ein Fedimin verpflichtet sich, keine kommerzielle Nutzung der Daten oder Metadaten der Nutzer_innen vornehmen.
- Ein Fedimin verpflichtet sich, die Handlungen der Nutzer_innen nicht zu überwachen, außer zu administrativen, technischen oder internen Verbesserungszwecken der Dienste
- Ein Fedimin verpflichtet sich, keine Vorabzensur der Inhalte der Nutzer_innen Seiten vorzunehmen
- Ein Fedimin verpflichtet sich, die Daten ihrer Nutzer_innen so gut wie möglich vor externen Angriffen zu schützen, insbesondere indem sie beim Empfang/bei der Übertragung über das Netz (SSL/TLS) oder bei der Speicherung (insbesondere von Dateien oder Datenbanken) so weit wie möglich eine starke Verschlüsselung der Daten verwendet
- Ein Fedimin verpflichtet sich, nicht auf eine Verwaltungs- oder Behördenanfrage zu reagieren, die die Bereitstellung von persönlichen Informationen erfordert, bevor ihm nicht eine ordnungsgemäße rechtliche Anfrage vorgelegt wurde

### Moderation

- Ein Fedimin verpflichtet sich, an einer leicht zugänglichen und zentralen Stelle klare und präzise Regeln und Richtlinien darüber veröffentlichen, wann Maßnahmen in Bezug auf die Inhalte oder Konten der Nutzer ergriffen werden
- Ein Fedimin verpflichtet sich, dass Menschenrechts- und Verfahrenserwägungen in allen Phasen des Prozesses der Inhaltsmoderation berücksichtigt werden
- Ein Fedimin verpflichtet sich, dass die Moderation durch Menschen durchgeführt wird und nicht über automatisierte Verfahren
- Ein Fedimin verpflichtet sich, folgende Punkte aktiv[^3] zu Moderieren:
  - alle Arten von Diskriminierung oder deren Befürwortung
  - Sexualisierte Darstellungen von Kindern
  - Nationalistische Propaganda, Nazi-Symbolismus oder Förderung der Ideologie des Nationalsozialismus oder
  - extremistische und radikale Propaganda sowie deren Ideologien
- Ein Fedimin verpflichtet sich, bei der Moderation ist immer das mildeste Mittel zu wählen, das andere User schützt, aber nicht behindert

## Singeluserinstanzen[^4]

- Ein Fedimin verpflichtet sich, die Basis Moderationsrichtlinien einzuhalten

Beinhaltet Teile aus "[Charter of CHATONS](https://www.chatons.org/charte)" von CHATONS. Der Text ist lizenziert unter der [BY-SA 4.0 Lizenz](https://creativecommons.org/licenses/by-sa/4.0/).

[^1]: Die im Folgenden als "Nutzer_innen" bezeichneten sind natürliche oder juristische Personen, die die Möglichkeit haben, Daten auf der Infrastruktur zu erstellen oder zu ändern.

[^2]: Ein Hosting-Anbieter wird hier definiert als eine Einheit, die von Dritten bereitgestellte Daten beherbergt und Dienstleistungen anbietet, über die diese Daten laufen oder gespeichert werden.

[^3]: Aktiv bedeutet, dass Meldungen oder Beiträge die ihr entdeckt, schnellstmöglich moderiert werden; es verpflichtet nicht zur manuellen Durchsuchung der Beiträge eurer Instanz.

[^4]: Dabei handelt es sich um Instanzen, die nur einen bzw. ein paar User(z.B. eine Instanz für die Familie) beherbergen.
