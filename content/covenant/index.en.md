---
title: Covenant
description: This is the pre-release version of the Fedimins server agreement
date: 2020-09-01T00:00:00.000Z
lastmod: 2023-01-16T17:52:00.133Z
tags:
  - ""
categories:
  - ""
slug: covenant
---

Fedimins is a collective of alternative, transparent, open, neutral and solidary hosting providers. Each member of this collective, hereinafter referred to as "Fedimin", undertakes to respect this Charter. What is not explicitly prohibited by this Charter or by law is allowed. The Fedimin may have any legal form: a legal entity or a natural person (an individual, a company, an association, a SCOP, etc.).

**Requirements**

- the Fedimin commits to act with respect and goodwill towards the other members of the collective
- the Fedimin commits to act with respect and goodwill towards the users[^1] of its services

## Basic commitments

### Hosting

- the Fedimin must have administrator access (root access) to the operating system that runs the final online services
  - In case of renting servers (virtual or dedicated), the Fedimin must ensure that the provider contractually undertakes not to access the data
- the Fedimin commits to ensure that at least two different (natural) persons are granted administrator access (root access) to the operating system.
- the Fedimin commits to inform its users about the degree of control it has over its infrastructure.
- the Fedimin commits to publicly and unambiguously display its degree of control over the hardware and software hosting the services and related data. In particular, the name of the hosting provider[^2] that physically hosts the servers must be clearly indicated to the users_.
- the Fedimin commits to implement the backup policy that allows for daily backups
- the Fedimin commits to use only free distributions (GNU/Linux, FreeBSD, etc.) as the operating system for the infrastructure of the user services.
- the Fedimin commits to permanently delete all information (accounts and personal data) concerning the user, at the user's request, within the limits of the legal and technical obligations.

### Transparency

- the Fedimin commits to make its technical infrastructure (distribution and software used) transparent. For security reasons, certain information (e.g. the version number of the software) cannot be made publicly available.
- the Fedimin commits to do its best to ensure the security of its infrastructure.
- the Fedimin commits not to claim ownership of the content, data and metadata produced by its users
- the Fedimin commits to publish terms of use that are visible, clear, unambiguous and universally understood.
- the Fedimin commits to notify users at least 3 months prior to shutting down the services.

### Privacy policy

- the Fedimin commits to prioritize the fundamental freedoms of its users, in particular the protection of their privacy, in all its actions.
- the Fedimin commits not, under any circumstances, use the services of advertising agencies. Sponsorship or patronage through the display of the identity of partner structures (name, logo, etc.) is permitted, provided that no personal information is disclosed to the partners
- the Fedimin commits not make any commercial use of the data or metadata of the users.
- the Fedimin commits not to monitor the actions of users, except for administrative, technical or internal improvement purposes of the services.
- the Fedimin commits not to carry out any prior censorship of the contents of the user's pages.
- the Fedimin commits to protect the data of its users as much as possible from external attacks, in particular by using strong encryption of the data when receiving/transmitting it over the network (SSL/TLS) or when storing it (in particular files or databases).
- the Fedimin agrees not to respond to any administrative or governmental request requiring the provision of personal information before being presented with a proper legal request

### Moderation

- the Fedimin commits to publish, in an easily accessible and centralized location, clear and precise rules and guidelines on when action will be taken in relation to users' content or accounts
- the Fedimin commits that human rights and procedural considerations are taken into account at all stages of the content moderation process
- the Fedimin commits that moderation will be carried out by humans and not through automated processes
- the Fedimin commits to actively[^3] moderate the following:
  - All types of discrimination or advocacy thereof.
  - Sexualized depictions of children
  - Nationalist propaganda, Nazi symbolism or promotion of the ideology of National Socialism, or
  - Extremist and radical propaganda and their ideologies.
- the Fedimin commits itself to always use the mildest means of moderation that protects other users but does not hinder them.

## Singeluserin instances[^4]

- the Fedimin agrees to abide by the basic moderation guidelines.

Includes portions from "[Charter of CHATONS](https://www.chatons.org/charte)" by CHATONS. This text is licensed under a [BY-SA 4.0 License](https://creativecommons.org/licenses/by-sa/4.0/).

[^1]: Hereafter referred to as "users" are natural or legal persons who have the ability to create or modify data on the infrastructure.

[^2]: A hosting provider is defined here as an entity that hosts data provided by third parties and provides services through which this data runs or is stored.

[^3]: Active means that messages or posts you discover will be moderated as soon as possible; it does not obligate you to manually search your instance's posts.

[^4]: These are instances that host only one or a few users(e.g. a family instance).
