if (window.matchMedia('(prefers-color-scheme: dark)').matches) {
  document.documentElement.setAttribute("data-bs-theme", "dark");
  document.documentElement.setAttribute("bs-theme", "auto");
}

function set_light() {
  document.documentElement.setAttribute('data-bs-theme', 'light');
}

function set_dark() {
  document.documentElement.setAttribute('data-bs-theme', 'dark');
}

function set_auto() {
  if (window.matchMedia("(prefers-color-scheme: dark)").matches) {
    document.documentElement.setAttribute("data-bs-theme", "dark");
  } else {
    document.documentElement.removeAttribute("data-bs-theme");
    document.documentElement.setAttribute("bs-theme", "auto");
  }
}
